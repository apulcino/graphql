git init
git add README.md
git commit -m "first commit"

git remote add origin https://apulcino@bitbucket.org/apulcino/graphql.git
git push -u origin master

Samples:
git clone https://github.com:sitepoint-editors/node-elasticsearch-tutorial.git
cd node-elasticsearch-tutorial
npm install

GraphQL:
npm install express express-graphql graphql --save

const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

http://localhost:4000/graphql