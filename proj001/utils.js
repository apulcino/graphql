module.exports.asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

  module.exports.bodySearch = {
    query:{
      bool:{
        must:[{match_all:{}}],
        must_not:[],
        should:[]
      }
    },
    from:0,
    size:10,
    sort:["account_number"],
    aggs:{}
  }