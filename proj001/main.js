const express = require('express')
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');

const app = express()
const data = require('./data')
const utils = require('./utils');

var elasticsearch = require('elasticsearch');
var esClient = new elasticsearch.Client({
  host: 'elasticsearch:9200',
  log: 'trace',
  apiVersion: '6.8', // use the same version of your Elasticsearch instance
});

const esSearch = function search(body) {
  return esClient.search({
    index: 'afphub-db',
    type: 'documents',
    body: body});
};

function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}

app.use(logErrors);
app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.get('/checkES', function (req, res) {
  esClient.ping({
    requestTimeout: 1000
  }, function (error) {
    if (error) {
      res.send('elasticsearch cluster is down!');
    } else {
      res.send('All is well');
    }
  });  
})

app.get('/search', utils.asyncMiddleware(async (req, res, next) => {
  let response = await esSearch(utils.bodySearch)
  res.json(response.hits.hits);
}))

app.get('/users0', function (req, res) {
    data.AFPResponse.status.code = 200;
    data.AFPResponse.status.message = "";
    data.AFPResponse.data = data.Users
    res.json(data.AFPResponse)
  })

app.get('/users', function (req, res) {
    var data = [];
    esSearch(utils.bodySearch)
    .then(results => {
      console.log(`found ${results.hits.total} items in ${results.took}ms`);
      console.log(`returned article titles:`);
      results.hits.hits.forEach(
        (hit, index) => { 
          console.log(`\t${utils.bodySearch.from + ++index} - ${hit._source.firstname} ${hit._source.lastname}`);
          data.push(`${hit._source.firstname} ${hit._source.lastname}`)
        }
      )
      res.json(data)
    })
    .catch(console.error);
  })


app.get('/setuserfn/:id/:firstname', (req, res) => {
    console.log  (JSON.stringify(req.params))

    if (req.params.id >= data.Users.length) {
      res.status(500)
      res.send('Something broke!');
    } else {
      var obj = data.Users[req.params.id - 1]
      console.log(JSON.stringify(obj))
      obj.firstname = req.params.firstname  
      console.log(JSON.stringify(obj)) 
      data.AFPResponse.status.code = 200;
      data.AFPResponse.status.message = "";
      data.AFPResponse.data = obj
      res.status(200)
      res.json(data.AFPResponse)
    }

  })

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type User {
    firstname: String
    lastname: String
    age: Int
  }  

  type Query {
    hello: String
    users:[User]
  }

`);

// The root provides a resolver function for each API endpoint
// http://localhost:4000/graphql?query={hello}
// http://localhost:4000/graphql?query={users}
var root = {
  hello: () => {
    return 'Hello world!';
  },
  /*
  users: () => {
    return [{  
      firstname: 'a',
      lastname: 'a'
    },{
      firstname: 'b',
      lastname: 'b'
    }]
  },
  */
  
  users: async () => {
    var data = [];
    let results = await esSearch(utils.bodySearch)
    console.log(`found ${results.hits.total} items in ${results.took}ms`);
    results.hits.hits.forEach(
      (hit, index) => { 
        console.log(`\t${utils.bodySearch.from + ++index} - ${hit._source.firstname} ${hit._source.lastname}`);
        data.push({
          firstname : hit._source.firstname,
          lastname: hit._source.lastname,
          age: hit._source.age
        })
      }
    )
    return data
  },
};

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(4000, function () {
  console.log('Example app listening on port 4000!')
})

